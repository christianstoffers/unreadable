#!/usr/bin/env bash

# constant definition
_FILENAME=message.txt
SALT="${SALT:="choosed a weak salt, you have"}"

NO_COLOR='\033[0m'
RED='\033[0;31m'
CYAN='\033[0;36m'
PURPLE='\033[0;35m'
YELLOW='\033[1;33m'
BLUE='\033[0;34m'
GREEN='\033[0;32m'

# function definition
printUsage() {
  printf "usage: [SALT='text'] [DEBUG=[true|false]] $0 ['message'] [OPTION]\n"
  printf "usage: $0 --help\n"
  echo ""
}

printDetailedUsage() {
  printf "${CYAN}ENCODE WITH GIVEN TEXT (PLAIN TEXT FORMAT)${NO_COLOR}\n"
  printf "${CYAN}•${NO_COLOR} usage: bash $0 \'given text\'\n"
  printf "${CYAN}•${NO_COLOR} usage: SALT='text' bash $0 \'given text\'\n"
  echo ""
  printf "${CYAN}ENCODE FROM FILE${NO_COLOR}\n"
  printf "${CYAN}•${NO_COLOR} paste plain text message into the file ${YELLOW}$_FILENAME${NO_COLOR}\n"
  printf "${CYAN}•${NO_COLOR} usage: bash $0 -f\n"
  printf "${CYAN}•${NO_COLOR} usage: bash $0 --file\n"
  printf "${CYAN}•${NO_COLOR} usage: SALT='text' bash $0 --file\n"
  echo ""
  printf "${PURPLE}DECODE WITH GIVEN STRING (base64 FORMAT)${NO_COLOR}\n"
  printf "${PURPLE}•${NO_COLOR} usage: bash $0 \'given text\' -d\n"
  printf "${PURPLE}•${NO_COLOR} usage: bash $0 \'given text\' --decode\n"
  printf "${PURPLE}•${NO_COLOR} usage: SALT='text' bash $0 \'given text\' --decode\n"
  echo ""
  printf "${PURPLE}DECODE FROM FILE${NO_COLOR}\n"
  printf "${PURPLE}•${NO_COLOR} paste encoded string into the file ${YELLOW}$_FILENAME${NO_COLOR}\n"
  printf "${PURPLE}•${NO_COLOR} usage: bash $0 -d -f\n"
  printf "${PURPLE}•${NO_COLOR} usage: bash $0 --decode --file\n"
  printf "${PURPLE}•${NO_COLOR} usage: SALT='text' bash $0 --decode --file\n"
  echo ""
}

printUsageHeader() {
  printf "${BLUE}++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++${NO_COLOR}\n"
  printf "${BLUE}USAGE:${NO_COLOR}\n"
}

printHelp() {
  echo "Use it to encode a message into a not human readable format. Use it also "
  echo "to decode a message which was previously encoded with this script. This "
  echo "seems handy when needed to transfer a message easily via a not trustworthy "
  echo "way to add a little bit more of security."
  echo ""
  echo "The primary objectives are:"
  echo "- to secure the secret from a person standing behind you, while handling "
  echo "  the secret"
  echo "- to use a password (SALT) to decode the secret"
  echo "- to reduce the number of persons capable of decoding the secret because"
  echo "  the person must be in possession of the script to decode the secret."
  echo ""
  printf "${RED}IMPORTANT:${NO_COLOR} The message will be ${RED}not${NO_COLOR} encrypted. It is just encoded \n"
  printf "in a different way. The message will be scrambled a bit, so a simple \n"
  printf "base64 decoding will protect the content of the message.\n"
  printf "But the alghorithm is very unsecure and should not be called a encryption.\n"
  echo ""
  printf "${YELLOW}NOTICE:${NO_COLOR} If you want to use a [!] in your message to ${CYAN}ENCODE${NO_COLOR} it, use single \n"
  printf "quotes [']. With double quotes [\"] the [!] will act as a keyword for bash \n"
  printf "to search in the history for a command used in the past.\n"
  printf "A typical error message looks like this:\n"
  printf "bash: !test: event not found\n"
  echo ""
  printf "${GREEN}OPTIONS:${NO_COLOR} \n"
  printf "  -d, --decode \t decoding of base64 encoded text \n"
  printf "  -f, --file \t encodes or decodes the message from file: ${_FILENAME} \n"
  echo ""
}

printDebug() {
  if [ "$DEBUG" == true ]; then
    printf "${BLUE}### DEBUG:${NO_COLOR}\n"
    echo "$1"
  fi
}

checkContent() {
  if [ "$1" == "hello there" ] || [ "$1" == "Hello there" ]; then
    printf "${GREEN}General Kenobi:${NO_COLOR} Hello there\n"
    printf "${RED}General Grevious:${NO_COLOR} "
    read INPUT

    if [ "$INPUT" == "General Kenobi" ] || [ "$INPUT" == "general kenobi" ]; then
      sleep 2
      printf "${RED}General Grevious:${NO_COLOR} You are a bold one.\n"
      sleep 4
      printf "${RED}General Grevious:${NO_COLOR} Kill him!\n"
      sleep 3
      printf "${BLUE}General Kenobi destroying some droids.${NO_COLOR}\n"
      sleep 2
      printf "${BLUE}General Kenobi destroying some droids..${NO_COLOR}\n"
      sleep 2
      printf "${BLUE}General Kenobi destroying some droids...${NO_COLOR}\n"
      sleep 2
      printf "${RED}General Grevious:${NO_COLOR} Back away! I will deal with this Jedi slime myself.\n"
      sleep 5
      printf "${GREEN}General Kenobi:${NO_COLOR} Your move.\n"
      sleep 3
      printf "${RED}General Grevious:${NO_COLOR} You fool. I've been trained in your Jedi arts by Count Dooku.\n"
      sleep 6
      printf "${RED}General Grevious:${NO_COLOR} Attack, Kenobi!\n"
      sleep 3
      printf "${BLUE}General Grevious starts rotating his lightsabers...${NO_COLOR}\n"
      sleep 4
      echo '      ________________.  ___     .______'
      sleep 1
      echo '     /                | /   \    |   _  \'
      echo '    |   (-----|  |----`/  ^  \   |  |_)  |'
      sleep 1
      echo '     \   \    |  |    /  /_\  \  |      /'
      echo '.-----)   |   |  |   /  _____  \ |  |\  \-------.'
      sleep 1
      echo '|________/    |__|  /__/     \__\| _| `.________|'
      echo ' ____    __    ____  ___     .______    ________.'
      sleep 1
      echo ' \   \  /  \  /   / /   \    |   _  \  /        |'
      echo '  \   \/    \/   / /  ^  \   |  |_)  ||   (-----`'
      sleep 1
      echo '   \            / /  /_\  \  |      /  \   \'
      echo '    \    /\    / /  _____  \ |  |\  \---)   |'
      sleep 1
      echo '     \__/  \__/ /__/     \__\|__| `._______/'
      echo ''
      sleep 1
      echo '------------------------------------------------'
      echo ''
      echo ''
      exitScript

      else
        sleep 1
        printf "${RED}❌${BLUE}Thats not what he said... Try again.${NO_COLOR}\n"
    fi
    exitScript
  fi
}

exitScript() {
  exit 1
}

errorWhenFirstDecodingIsNotPossible() {
  echo "$_MESSAGE_CONTENT_ONE_LINE" | base64 --decode &>/dev/null

  if [[ $? -gt 0 ]]; then
    printf "${RED}ERROR:${NO_COLOR}\tDecoding not possible. Invalid INPUT.\n"
    printf "\tExpected: ${YELLOW}base64 encoded text${NO_COLOR}\n"
    printf "\tActual:   ${YELLOW}${_MESSAGE_CONTENT_ONE_LINE}${NO_COLOR}\n\n"
    exitScript
  fi
}

errorWhenCuttingIsNotPossible() {
  echo "$_FIRST_TIME_DECODED" | cut -b $_SALT_END_POSITION-"$_FIRST_TIME_DECODED_LENGTH" &>/dev/null

  if [[ $? -gt 0 ]]; then
    printf "${RED}ERROR:${NO_COLOR}\tProcessing decoding is not possible. Perhaps forgot to define ${YELLOW}salt${NO_COLOR}?\n"
    printf "\tusage: ${YELLOW}SALT='text represents salt'${NO_COLOR} $0 --decode [--file]\n"
    exitScript
  fi
}

errorWhenSecondDecodingIsNotPossible() {
  echo "$_FIRST_TIME_DECODED_WITHOUT_PREFIX" | base64 --decode &>/dev/null

    if [[ $? -gt 0 ]]; then
      printf "${RED}ERROR:${NO_COLOR}\tDecoding not possible. Maybe defined wrong ${YELLOW}salt${NO_COLOR}?\n"
      printf "\tSALT is: ${YELLOW}${SALT}${NO_COLOR}\n"
      exitScript
    fi
}

encodeMessage() {
  _MESSAGE_CONTENT_ORIGINAL=$1
  _MESSAGE_CONTENT_ONE_LINE=$(echo "$_MESSAGE_CONTENT_ORIGINAL" | tr --delete '\n')
  checkContent "$_MESSAGE_CONTENT_ONE_LINE"
  _FIRST_TIME_ENCODED=$(echo "$_MESSAGE_CONTENT_ONE_LINE" | base64 | tr --delete '\n')
  _FIRST_TIME_ENCODED_WITH_PREFIX="$SALT$_FIRST_TIME_ENCODED"
  _SECOND_TIME_ENCODED=$(echo "$_FIRST_TIME_ENCODED_WITH_PREFIX" | base64 | tr --delete '\n')

  printDebug "_MESSAGE_CONTENT_ORIGINAL: $_MESSAGE_CONTENT_ORIGINAL"
  printDebug "_MESSAGE_CONTENT_ONE_LINE: $_MESSAGE_CONTENT_ONE_LINE"
  printDebug "_FIRST_TIME_ENCODED: $_FIRST_TIME_ENCODED"
  printDebug "_FIRST_TIME_ENCODED_WITH_PREFIX: $_FIRST_TIME_ENCODED_WITH_PREFIX"
  printDebug "_SECOND_TIME_ENCODED: $_SECOND_TIME_ENCODED"

  echo "> encoded message:"
  echo "${_SECOND_TIME_ENCODED}"
  exitScript
}

decodeMessage() {
  _MESSAGE_CONTENT_ORIGINAL=$1
  _MESSAGE_CONTENT_ONE_LINE=$(echo "$_MESSAGE_CONTENT_ORIGINAL" | tr --delete '\n')
  errorWhenFirstDecodingIsNotPossible
  _FIRST_TIME_DECODED=$(echo "$_MESSAGE_CONTENT_ONE_LINE" | base64 --decode | tr --delete '\n')
  _FIRST_TIME_DECODED_LENGTH="${#_FIRST_TIME_DECODED}"
  _SALT_END_POSITION="$((${#SALT} + 1))"
  errorWhenCuttingIsNotPossible
  _FIRST_TIME_DECODED_WITHOUT_PREFIX=$(echo "$_FIRST_TIME_DECODED" | cut -b $_SALT_END_POSITION-"$_FIRST_TIME_DECODED_LENGTH")
  errorWhenSecondDecodingIsNotPossible
  _SECOND_TIME_DECODED=$(echo "$_FIRST_TIME_DECODED_WITHOUT_PREFIX" | base64 --decode | tr --delete '\n')

  printDebug "_MESSAGE_CONTENT_ORIGINAL: ${_MESSAGE_CONTENT_ORIGINAL}"
  printDebug "_MESSAGE_CONTENT_ONE_LINE: ${_MESSAGE_CONTENT_ONE_LINE}"
  printDebug "_FIRST_TIME_DECODED: ${_FIRST_TIME_DECODED}"
  printDebug "_FIRST_TIME_DECODED_LENGTH: ${_FIRST_TIME_DECODED_LENGTH}"
  printDebug "_SALT_END_POSITION: ${_SALT_END_POSITION}"
  printDebug "_FIRST_TIME_DECODED_WITHOUT_PREFIX: ${_FIRST_TIME_DECODED_WITHOUT_PREFIX}"
  printDebug "_SECOND_TIME_DECODED: ${_SECOND_TIME_DECODED}"

  echo "> decoded message:"
  echo "${_SECOND_TIME_DECODED}"
  exitScript
}


#        _______.___________.    ___      .______     .___________.
#       /       |           |   /   \     |   _  \    |           |
#      |   (----`---|  |----`  /  ^  \    |  |_)  |   `---|  |----`
#       \   \       |  |      /  /_\  \   |      /        |  |
#   .----)   |      |  |     /  _____  \  |  |\  \----.   |  |
#   |_______/       |__|    /__/     \__\ | _| `._____|   |__|

if [[ $# -eq 0  ]]; then
  printUsage
  exitScript
fi
if [ "$1" == "--help" ] || [ "$1" == "-h" ]; then
  printHelp
  printUsageHeader
  printUsage
  printDetailedUsage
  exitScript
fi
if [[ $# -gt 2 ]]; then
  printf "${RED}ERROR:${NO_COLOR}\tExpected maximum amount of parameters is: ${YELLOW}'2'${NO_COLOR}. Actual: ${YELLOW}'$#'${NO_COLOR}\n\n"
  sleep 2
  printUsage
  printDetailedUsage
  exitScript
fi
if { [ "$1" == "-d" ] || [ "$1" == "--decode" ]; } && [[ $# -eq 1 ]]; then
  printf "${RED}ERROR:${NO_COLOR}\tWrong amount of parameters. When decoding there are at least ${YELLOW}2${NO_COLOR} parameters expected.\n"
  echo ""
  printUsage
  sleep 2
  printDetailedUsage
fi

# ---------------------------------------------------------------------------
# unreadable.sh 'text'    # encode from parameter
if { [ "$1" != "" ] && [ "$1" != "-d" ] && [ "$1" != "--decode" ] && [ "$1" != "-f" ] && [ "$1" != "--file" ]; } \
  && { [[ $# -eq 1 ]]; }; then
  printDebug "ENCODING WITH GIVEN TEXT | text: $1"
  encodeMessage "$1"
fi

# ---------------------------------------------------------------------------
# unreadable.sh -f           # encode from file
if { [ "$1" == "-f" ] || [ "$1" == "--file" ]; } \
  && { [[ $# -eq 1 ]]; }; then
  _FILE_CONTENT=$(<$_FILENAME)
  printDebug "ENCODING FROM FILE | filecontent: $_FILE_CONTENT"
  encodeMessage "$_FILE_CONTENT"
fi

# ---------------------------------------------------------------------------
# unreadable.sh -d 'text' # decode from parameter
if { [ "$1" == "-d" ] || [ "$1" == "--decode" ]; } \
  && { [ "$2" != "" ] && [ "$2" != "-f" ] \
  && [ "$2" != "--file" ]; } \
  && { [[ $# -eq 2 ]]; }; then
  printDebug "DECODING WITH GIVEN TEXT | text: $2"
  decodeMessage "$2"
fi
## unreadable.sh 'text' -d # decode from parameter
if { [ "$1" != "" ] && [ "$1" != "-f" ] && [ "$1" != "--file" ]; } \
  && { [ "$2" == "-d" ] || [ "$2" == "--decode" ]; } \
  && { [[ $# -eq 2 ]]; }; then
  printDebug "DECODING WITH GIVEN TEXT | text: $1"
  decodeMessage "$1"
fi

# ---------------------------------------------------------------------------
# unreadable.sh -d -f        # decode from file
if { [ "$1" == "-d" ] || [ "$1" == "--decode" ]; } \
  && { [ "$2" == "-f" ] || [ "$2" == "--file" ]; }; then
  _FILE_CONTENT=$(<$_FILENAME)
  printDebug "DECODING FROM FILE | filecontent: $_FILE_CONTENT"
  decodeMessage "$_FILE_CONTENT"
fi
# unreadable.sh -f -d        # decode from file
if { [ "$1" == "-f" ] || [ "$1" == "--file" ]; } \
  && { [ "$2" == "-d" ] || [ "$2" == "--decode" ]; }; then
  _FILE_CONTENT=$(<$_FILENAME)
  printDebug "DECODING FROM FILE | filecontent: $_FILE_CONTENT"
  decodeMessage "$_FILE_CONTENT"
fi
