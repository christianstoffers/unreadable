# description

Use it to encode a message into a not human readable format. Use it also to decode a message which
was previously encoded with this script. This seems handy when needed to transfer a message easily
via a not trustworthy way to add a little bit more of security.

---

# help

Open this help content via script with...

```shell
bash unreadable.sh --help
```

## the primary objectives are

- to secure the secret from a person standing behind you, while handling the secret
- to use a password (SALT) to decode the secret
- to reduce the number of persons capable of decoding the secret because the person must be in
  possession of the script to decode the secret.

### important

The message will be **not** encrypted. It is just encoded in a different way. The message will be
scrambled a bit, so a simple base64 decoding will protect the content of the message. But the
alghorithm is very unsecure and should not be called a encryption.

### notice

If you want to use a `[!]` in your message to ENCODE it, use single quotes `[']`. With double
quotes `["]` the `[!]` will act as a keyword for bash to search in the history for a command used in
the past.

A typical **error message** looks like this: `bash: !test: event not found`

### options

decoding of base64 encoded text

```
-d, --decode
```

encodes or decodes the message from file: message.txt

```
-f, --file
```

---

# usage

```shell
[SALT='text'] [DEBUG=[true|false]] unreadable.sh ['message'] [OPTION]
```

## encode with given text (plain text format)

```shell
bash unreadable.sh 'given text'
SALT='text' bash unreadable.sh 'given text'
```

## encode from file

paste plain text message into the file `message.txt`

```shell
bash unreadable.sh -f
bash unreadable.sh --file
SALT='text' bash unreadable.sh --file
```

## decode with given string (base64 format)

```shell
bash unreadable.sh 'given text' -d
bash unreadable.sh 'given text' --decode
SALT='text' bash unreadable.sh 'given text' --decode
```

## decode from file

paste encoded string into the file `message.txt`

```shell
bash unreadable.sh -d -f
bash unreadable.sh --decode --file
SALT='text' bash unreadable.sh --decode --file
```
